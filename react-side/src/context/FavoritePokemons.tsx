import React, { createContext, useState } from "react";
import { Pokemon } from "../api/pokemon";

interface FavoritePokemonsContextTypes {
    favoritePokemons: Pokemon[],
    setFavoritePokemons: React.Dispatch<React.SetStateAction<Pokemon[]>>
}

export const FavoritePokemonsContext = createContext<FavoritePokemonsContextTypes>({
    favoritePokemons: [],
    setFavoritePokemons: () => { }
})

export default function FavoritePokemonsProvider({ children }: React.PropsWithChildren) {
    const [favoritePokemons, setFavoritePokemons] = useState<Pokemon[]>([])

    const value = {
        favoritePokemons,
        setFavoritePokemons
    }
    return (
        <FavoritePokemonsContext.Provider value={value}>
            {children}
        </FavoritePokemonsContext.Provider>
    )
}
