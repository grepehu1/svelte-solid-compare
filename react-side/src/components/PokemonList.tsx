import { useContext, useEffect, useState } from 'react';
import { getPokemonList, Pokemon } from '../api/pokemon';
import { FavoritePokemonsContext } from '../context/FavoritePokemons';

interface PokemonListProps {
    type: 'all' | 'favorites'
}

interface PokemonListItemProps {
    pokemon: Pokemon
}


const PokemonListItem = ({ pokemon }: PokemonListItemProps) => {
    const { favoritePokemons, setFavoritePokemons } = useContext(FavoritePokemonsContext)

    function toggleFavoritePokemon(pokemon: Pokemon, isFav: boolean) {
        if (isFav) {
            setFavoritePokemons((prev) => {
                return prev.filter((item) => item.url !== pokemon.url);
            })
        } else {
            setFavoritePokemons((prev) => {
                return [...prev, pokemon]
            })
        }
    }

    const baseClass = "cursor-pointer px-4 py-2 hover:text-sky-900 border-b last:border-none border-gray-200 transition-all duration-300 ease-in-out"
    const isFav = !!favoritePokemons.find(favPok => favPok.url === pokemon.url)
    const bgClass = isFav ? ' bg-red-300 hover:bg-red-100' : ' bg-white hover:bg-sky-100'

    const finalClass = baseClass + bgClass

    return (
        <li className={finalClass} onClick={() => toggleFavoritePokemon(pokemon, isFav)}>
            {pokemon.name}
        </li>
    )
}

export const PokemonList = ({ type }: PokemonListProps) => {
    const [pokemons, setPokemons] = useState<Pokemon[]>([])
    const { favoritePokemons } = useContext(FavoritePokemonsContext)

    async function fetchPokemons() {
        const data = await getPokemonList()
        if (data?.length) {
            setPokemons(data)
        }
    }
    useEffect(() => {
        fetchPokemons()
    }, [])

    return (
        <ul className="border border-gray-200 rounded overflow-hidden shadow-md">
            {type === 'all' ? (
                <>{pokemons.map(pokemon => <PokemonListItem key={pokemon.url} pokemon={pokemon} />)}</>
            ) : type === 'favorites' ? (
                <>{favoritePokemons.map(pokemon => <PokemonListItem key={pokemon.url} pokemon={pokemon} />)}</>
            ) : <></>}
        </ul>
    )
}
