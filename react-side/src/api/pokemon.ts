
export type Pokemon = {
    name: string
    url: string
}

export async function getPokemonList(limit = 151, offset = 0): Promise<Pokemon[]> {
    const pokeUrl = `https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`
    const res = await fetch(pokeUrl)
    const data = await res?.json()

    return data?.results || []
}