import { createStore } from "solid-js/store";
import { Pokemon } from "../api/pokemon";

const FavoritePokemonStore = createStore<Pokemon[]>([])

export default FavoritePokemonStore