import { Component, createResource, For, Switch, Match, createSignal, createEffect } from 'solid-js';
import { getPokemonList, Pokemon } from '../api/pokemon';
import FavoritePokemonStore from '../stores/FavoritePokemons';

const [favPokemons, setFavPokemons] = FavoritePokemonStore

interface PokemonListProps {
    type: 'all' | 'favorites'
}

interface PokemonListItemProps {
    pokemon: Pokemon
}

function toggleFavoritePokemon(pokemon: Pokemon, isFav: boolean) {
    if (isFav) {
        setFavPokemons((prev) => {
            return prev.filter((item) => item.url !== pokemon.url);
        })
    } else {
        setFavPokemons((prev) => {
            return [...prev, pokemon]
        })
    }
}

const PokemonListItem = (props: PokemonListItemProps) => {
    const [thisPok, setThisPok] = createSignal(props.pokemon)
    const [fClass, setFClass] = createSignal('')
    const [isFav, setIsFav] = createSignal(false)

    createEffect(() => {
        const newIsFav = !!favPokemons.find(favPok => favPok.url === thisPok().url)
        setIsFav(newIsFav)
    })

    createEffect(() => {
        const baseClass = "cursor-pointer px-4 py-2 hover:text-sky-900 border-b last:border-none border-gray-200 transition-all duration-300 ease-in-out"
        const bgClass = isFav() ? ' bg-red-300 hover:bg-red-100' : ' bg-white hover:bg-sky-100'

        const finalClass = baseClass + bgClass

        setFClass(finalClass)
    })

    return (
        <li class={fClass()} onClick={() => toggleFavoritePokemon(thisPok(), isFav())}>
            {props.pokemon.name}
        </li>
    )
}

export const PokemonList = (props: PokemonListProps) => {
    const [data, { mutate, refetch }] = createResource(() => getPokemonList())

    // Handle Loading
    // data.loading

    // Handle error
    // data.error

    return (
        <ul class="border border-gray-200 rounded overflow-hidden shadow-md">
            <Switch fallback={<div>Not Found</div>}>
                <Match when={props.type === "all"}>
                    <For each={data()} >{(pokemon) => {
                        return <PokemonListItem pokemon={pokemon} />
                    }}</For>
                </Match>
                <Match when={props.type === "favorites"}>
                    <For each={favPokemons} >{(pokemon) => {
                        return <PokemonListItem pokemon={pokemon} />
                    }}</For>
                </Match>
            </Switch>
        </ul>
    )
}
