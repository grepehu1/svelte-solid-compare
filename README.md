# Svelte x SolidJS Comparison

This is a simple repository to compare Svelte and SolidJS.

## Installation

You must have Node version >= 16.

To test, simply enter either the folders `solid-side` or `svelte-side` and run:

```bash
npm install
```

or if you have yarn install:

```bash
yarn install
```

## Usage

In either of the folders you may test the project with:

```bash
npm run dev
```
```bash
yarn dev
```

Or export the static files for side comparison with:


```bash
npm run build
```
```bash
yarn build
```

Where both projects export to a folder called `dist`.

## Comparsion So Far

### SolidJS

| Factor | Result |
| --- | --- |
| Number of lines | 152 |
| Final Dist Size | 27.7 kb |

### Svelte

| Factor | Result |
| --- | --- |
| Number of lines | 155 |
| Final Dist Size | 19.9 kb |

### React

| Factor | Result |
| --- | --- |
| Number of lines | 160 |
| Final Dist Size | 151.9 kb |