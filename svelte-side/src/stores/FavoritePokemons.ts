import { writable } from "svelte/store";
import type { Pokemon } from "../api/pokemon";

export const favoritePokemons = writable<Pokemon[]>([]);